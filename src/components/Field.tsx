import React, { useState} from 'react';
import { FieldExtensionSDK } from 'contentful-ui-extensions-sdk';
import { EditorToolbar, EditorToolbarButton, Button, Textarea, Pill, Dropdown, DropdownList, DropdownListItem} from '@contentful/forma-36-react-components';

// import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';

import '@contentful/forma-36-react-components/dist/styles.css';
import { SingleLineEditor } from '@contentful/field-editor-single-line';


interface FieldProps {
  sdk: FieldExtensionSDK;

}

// const options = [
//   'VERTRAGSNUMMER', 'VERTRAGSSTART', 'VERTRAGSENDE'
// ];
// const defaultOption = options[0];

const Field = (props: FieldProps) => {
  // If you only want to extend Contentful's default editing experience
  // reuse Contentful's editor components
  // -> https://www.contentful.com/developers/docs/extensibility/field-editors/
  // @ts-ignore

  const initialValue = ("Dein aktuellen Vertrag lautet:  ")

  const [contract, setContract] = useState("-");
  const [isOpen, setOpen] = useState(false);



  return <div>

      <EditorToolbar
      className={'className'}
      style={{ justifyContent: 'space-between' }}
    >
      <div>
        <EditorToolbarButton
          icon="FormatBold"
          tooltip="Bold"
          label="Bold"
          isActive={ false}
        />
        <EditorToolbarButton
          icon="FormatItalic"
          tooltip="Italic"
          label="Italic"
          isActive={false}
        />
        <EditorToolbarButton
          icon="FormatUnderlined"
          tooltip="Underlined"
          label="Underlined"
          isActive={false}
        />
        <EditorToolbarButton
          icon="Heading"
          tooltip="Heading"
          label="Heading"
          isActive={false}
        />
        
        <EditorToolbarButton
          icon="Code"
          tooltip="Code"
          label="Code"
          isActive={false}
        />
        
        <EditorToolbarButton
          icon="Link"
          tooltip="Link"
          label="Link"
          isActive={false}
        />

        <EditorToolbarButton
          icon="ListBulleted"
          tooltip="ListBulleted"
          label="ListBulleted"
          isActive={false}
        />
        
        <EditorToolbarButton
          icon="ListNumbered"
          tooltip="ListNumbered"
          label="ListNumbered"
          isActive={false}
        />

      <EditorToolbarButton
          icon="Quote"
          tooltip="Quote"
          label="Quote"
          isActive={false}
        />

      <EditorToolbarButton
          icon="HorizontalRule"
          tooltip="HorizontalRule"
          label="HorizontalRule"
          isActive={false}
        />
        
        
        



  <Dropdown
      isOpen={isOpen}
      onClose={() => setOpen(false)}
      toggleElement={
        <Button
          size="small"
          buttonType="muted"
          indicateDropdown
          onClick={() => setOpen(!isOpen)}
        >
          Vertrag auswählen
        </Button>
      }
    >
      <DropdownList>
        <DropdownListItem onClick={(event: any) => { setOpen(!isOpen); setContract((event.target as HTMLInputElement).innerText)}}>
        VERTRAGSNUMMER
        </DropdownListItem>
        <DropdownListItem onClick={(event: any) => { setOpen(!isOpen); setContract((event.target as HTMLInputElement).innerText)}}>
        VERTRAGSSTART
        </DropdownListItem>
        <DropdownListItem onClick={(event: any) => { setOpen(!isOpen); setContract((event.target as HTMLInputElement).innerText)}}>
        VERTRAGSENDE
        </DropdownListItem>
      </DropdownList>
    </Dropdown>

      </div>
      <div>
      
      </div>
    </EditorToolbar>

   

<Textarea
  name="someInput"
  id="someInput"
  width="full"
  rows={20}
  value={initialValue + contract}
  >
  </Textarea>



   





  </div>;

var e = document.getElementById("cars");
console.log(e);
  /*<textarea name="content" id="content"></textarea>*/
};

export default Field;




//  <input type="text" value={contract}/>