import React from "react";
import { EditorToolbar } from '@contentful/forma-36-react-components';

<EditorToolbar style={{ justifyContent: "space-between" }}>
  <div>
    <EditorToolbarButton
      icon="FormatBold"
      tooltip="Bold"
      label="Bold"
      isActive={false}
    />
    <EditorToolbarButton
      icon="FormatItalic"
      tooltip="Italic"
      label="Italic"
      isActive={false}
    />
    <EditorToolbarButton
      icon="FormatUnderlined"
      tooltip="Underlined"
      label="Underlined"
      isActive={false}
    />
  </div>
  <div>
    <Button size="small" buttonType="muted">
      Example button
    </Button>
  </div>
</EditorToolbar>;
